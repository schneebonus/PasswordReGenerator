package org.schneebonus.passwordregenerator.activities;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.schneebonus.passwordregenerator.DefaultValues;
import org.schneebonus.passwordregenerator.R;
import org.schneebonus.passwordregenerator.model.MainModel;

import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_main)
public class MainActivity extends RoboActivity {
    @InjectView(R.id.easy_password)         EditText easy_password;
    @InjectView(R.id.stronger_password)     TextView stronger_password;
    @InjectView(R.id.salt)                  EditText salt;
    @InjectView(R.id.equals)                Button calculate_password;
    @InjectView(R.id.copy)                  Button copy;
    @InjectView(R.id.info)                  ImageButton info;
    @InjectView(R.id.settings)              ImageButton settings;
    @InjectView(R.id.toogle_easy_password)  ImageView toggle_easypassword;
    @InjectView(R.id.toogle_salt)           ImageView toggle_salt;
    @InjectView(R.id.my_awesome_toolbar)    Toolbar toolbar;

    MainModel model = new MainModel(this);
    Activity activity = this;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null) {
            model = (MainModel) savedInstanceState.getSerializable(DefaultValues.SERIALIZATION_MODEL_NAME);
            model.setActivity(this);// TODO: dafuq is this shit: "may produce NullPointerException"?
        }

        setContent(model);
        init();
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putSerializable(DefaultValues.SERIALIZATION_MODEL_NAME, model);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    // -------------------- content management ------------------------//
    public void setContent(MainModel model){
        stronger_password.setText(model.getStrongerPassword());
        easy_password.setText(model.getPassword());
        salt.setText(model.getSalt());
    }

    private void setButtonEnabled(){
        if(salt.getText().toString().length() > 0 && easy_password.getText().toString().length() > 0){
            calculate_password.setEnabled(true);
        }
        else{
            calculate_password.setEnabled(false);
        }
    }

    private void init(){
        calculate_password.setOnClickListener(calculatePasswordListener);

        copy.setOnClickListener(copyListener);

        settings.setOnClickListener(settingsListener);

        info.setEnabled(false);
        info.setVisibility(ImageButton.INVISIBLE);

        toggle_easypassword.setOnClickListener(new PasswordTextToggeling(easy_password));
        toggle_salt.setOnClickListener(new PasswordTextToggeling(salt));

        salt.addTextChangedListener(checkForEmptyFieldsWatcher);
        easy_password.addTextChangedListener(checkForEmptyFieldsWatcher);
    }

    // ------------------ Listeners and Watchers -----------------------//
    // TODO: put them somewhere else. Dont want them here!

    TextWatcher checkForEmptyFieldsWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            setButtonEnabled();
        }

        @Override
        public void afterTextChanged(Editable s) {}
    };

    public class PasswordTextToggeling implements OnClickListener{
        private EditText toggleThis;
        boolean visible = false;

        // ctor
        public PasswordTextToggeling(EditText toggleThis){
            this.toggleThis = toggleThis;
        }

        @Override
        public void onClick(View v) {
            if (visible) {
                // hide password
                toggleThis.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                visible = false;
            } else {
                // show password
                toggleThis.setTransformationMethod(PasswordTransformationMethod.getInstance());
                visible = true;
            }
        }
    }

    OnClickListener calculatePasswordListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            model.calculate(easy_password.getText().toString(), salt.getText().toString());
        }
    };

    OnClickListener copyListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(DefaultValues.CLIPBOARD_NAME, stronger_password.getText().toString());
            clipboard.setPrimaryClip(clip);
        }
    };

    OnClickListener settingsListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(activity, Settings.class);
            startActivity(intent);
        }
    };

}
