package org.schneebonus.passwordregenerator.activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.schneebonus.passwordregenerator.DefaultValues;
import org.schneebonus.passwordregenerator.R;
import org.schneebonus.passwordregenerator.hashing.ImplementedHashingAlgorithms;

import java.util.ArrayList;
import java.util.List;

import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_settings)
public class Settings extends RoboActivity {
    @InjectView(R.id.algorithm_spinner)         Spinner algorithm_spinner;
    @InjectView(R.id.iterations_seekbar)        DiscreteSeekBar iterations_seekbar;
    @InjectView(R.id.back)                      ImageButton back;
    @InjectView(R.id.iterations_view)           TextView iterations_view;
    @InjectView(R.id.password_length_seekbar)   DiscreteSeekBar password_length_seekbar;
    @InjectView(R.id.password_length_view)      TextView password_length_view;

    Activity activity = this;
    enum seekerView {ITERATIONS, PASSWORD_LENGTH};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get properties
        SharedPreferences sharedPref = activity.getSharedPreferences(DefaultValues.PREFERENCES_NAME,Context.MODE_PRIVATE);
        String algo = sharedPref.getString(DefaultValues.PROPERTY_ALGORITHM, DefaultValues.DEFAULT_HASHING_ALHORITHM);
        int iterations = sharedPref.getInt(DefaultValues.PROPERTY_ITERATIONS, DefaultValues.DEFAULT_ITERATIONS);
        int password_length = sharedPref.getInt(DefaultValues.PROPERTY_PASSWORD_LENGTH, DefaultValues.DEFAULT_PASSWORD_LENGTH);

        // set spinner content
        List<String> list = new ArrayList<String>();

        // add all implemented algos to spinners content list
        for(String algo_name: ImplementedHashingAlgorithms.getImplenetedAlgorithmNames()){
            list.add(algo_name);
        }

        // set the spinners adapter
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,list);

        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);

        algorithm_spinner.setAdapter(dataAdapter);

        SpinnerAdapter adapter = algorithm_spinner.getAdapter();

        // set default value in spinner
        for(int i=0; i < adapter.getCount(); i++) {
            if(algo.equals(adapter.getItem(i).toString())){
                algorithm_spinner.setSelection(i);
                break;
            }
        }

        // set default values

        iterations_seekbar.setProgress(iterations);
        setIterationsView(iterations);

        password_length_seekbar.setProgress(password_length);
        setPasswordLengthView(password_length);


        // add listeners

        algorithm_spinner.setOnItemSelectedListener(algorithmListener);

        back.setOnClickListener(backListener);

        iterations_seekbar.setOnProgressChangeListener(
                new SeekbarChangePropertyListener(
                        DefaultValues.PROPERTY_ITERATIONS,
                        seekerView.ITERATIONS));

        password_length_seekbar.setOnProgressChangeListener(
                new SeekbarChangePropertyListener(
                        DefaultValues.PROPERTY_PASSWORD_LENGTH,
                        seekerView.PASSWORD_LENGTH));
    }

    private void setIterationsView(int value){
        iterations_view.setText("2^"+String.valueOf((int)value) +" = " + String.valueOf((int)Math.pow(2,value)));
    }

    private void setPasswordLengthView(int value){
        password_length_view.setText(String.valueOf(value) + " " + activity.getString(R.string.characters));
    }


    // ------------------ Listeners and Watchers -----------------------//
    // TODO: put them somewhere else. Dont want them here!

    AdapterView.OnItemSelectedListener algorithmListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String choosen_value = String.valueOf(algorithm_spinner.getSelectedItem());

            SharedPreferences sharedPref = activity.getSharedPreferences(DefaultValues.PREFERENCES_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();

            editor.putString(DefaultValues.PROPERTY_ALGORITHM, choosen_value);
            editor.commit();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    View.OnClickListener backListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            activity.finish();
        }
    };

    class SeekbarChangePropertyListener implements DiscreteSeekBar.OnProgressChangeListener{
        String propertyName;
        seekerView chooseView;

        public SeekbarChangePropertyListener(String propertyName, seekerView chooseView){
            this.propertyName = propertyName;
            this.chooseView = chooseView;
        }

        @Override
        public void onProgressChanged(DiscreteSeekBar discreteSeekBar, int i, boolean b) {
            int choosen_value = discreteSeekBar.getProgress();
            switch (chooseView){
                case ITERATIONS: setIterationsView(choosen_value); break;
                case PASSWORD_LENGTH: setPasswordLengthView(choosen_value); break;
            }
        }

        @Override
        public void onStartTrackingTouch(DiscreteSeekBar discreteSeekBar) {

        }

        @Override
        public void onStopTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            int choosen_value = discreteSeekBar.getProgress();

            SharedPreferences sharedPref = activity.getSharedPreferences(DefaultValues.PREFERENCES_NAME,Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();

            editor.putInt(propertyName, choosen_value);
            editor.commit();
        }
    }
}
