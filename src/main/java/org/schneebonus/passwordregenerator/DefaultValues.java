package org.schneebonus.passwordregenerator;

/**
 * Created by Mark Schneemann on 18.08.15.
 */
public class DefaultValues {
    // TODO: use these default values!
    public static int MAX_ITERATIONS = 16; // value x means 2^x iterations
    public static int MIN_PASSWORD_LENGTH = 6;
    public static int MAX_PASSWORD_LENGTH = 32;

    // Default values (if user did not change properties)
    public static String DEFAULT_HASHING_ALHORITHM = "SHA-512";
    public static int DEFAULT_ITERATIONS = 8;
    public static int DEFAULT_PASSWORD_LENGTH = 8;

    // statics for property names
    public static String PREFERENCES_NAME = "regenerator";
    public static String PROPERTY_ALGORITHM = "hashing_algorithm";
    public static String PROPERTY_ITERATIONS = "iterations";
    public static String PROPERTY_PASSWORD_LENGTH = "password_length";

    // serializations
    public static String SERIALIZATION_MODEL_NAME="lastmodel";

    // clipboard
    public static String CLIPBOARD_NAME = "copied hashed code";

    // init
    public static String INIT_STRING_VALUE = "not set";
}
