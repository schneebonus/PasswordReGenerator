package org.schneebonus.passwordregenerator.model;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Base64;
import android.widget.EditText;

import org.schneebonus.passwordregenerator.DefaultValues;
import org.schneebonus.passwordregenerator.R;
import org.schneebonus.passwordregenerator.activities.MainActivity;
import org.schneebonus.passwordregenerator.hashing.HashInterface;
import org.schneebonus.passwordregenerator.hashing.ImplementedHashingAlgorithms;

import java.io.Serializable;

/**
 * Created by Mark Schneemann on 18.08.15.
 */
public class MainModel implements Serializable {
    public enum Status {INIT, CALCULATING, RESULT, ERROR}

    private Status currentStatus = Status.INIT;
    private String strongerPassword = DefaultValues.INIT_STRING_VALUE;
    private String password = "";
    private String salt = "";

    transient private Activity activity; // has to be transient because it is not possible to
                                        // serialize the activity after user tabs out of the app!
                                       // transient means that it will not be serialized

    // default ctor
    public MainModel(Activity activity){
        this.activity = activity;
    }

    public void calculate(
            String password,
            String salt){

        if(currentStatus != Status.CALCULATING){
            // ready for calculation (no calculation thread is running)

            // TODO: save password and salt here of property is set

            // start the calculation thread!
            new CalculateHashTask().execute(password, salt);
        }
        else{
            // not ready for calculation. user should not be able do reach this -> error!
            setStatus(Status.ERROR);
        }


    };

    public void setActivity(Activity activity){
        this.activity = activity;
    }

    private void setStatus(Status newStatus){
        // change model status
        currentStatus = newStatus;
        // refresh gui
        ((MainActivity) activity).setContent(this);
    }

    public String getStrongerPassword(){
        // determine the text of the "stronger password"-line
        // depending on the current status
        switch (currentStatus) {
            case INIT: return activity.getString(R.string.stronger_password);
            case CALCULATING: return activity.getString(R.string.calculating);
            case RESULT: return strongerPassword;
            case ERROR: return activity.getString(R.string.error);
            default: return activity.getString(R.string.unknown_status_in_main_model);
        }
    }

    public String getPassword(){
        return password;
    }

    public String getSalt(){
        return salt;
    }

    private class CalculateHashTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute(){
            // set the status to CALCULATING.
            // this will prevent the user from starting another calculation
            // TODO: verify this!
            setStatus(MainModel.Status.CALCULATING);
        }

        @Override
        protected String doInBackground(String... params) {
            // get properties
            SharedPreferences sharedPref = activity.getSharedPreferences(DefaultValues.PREFERENCES_NAME, Context.MODE_PRIVATE);
            String algo = sharedPref.getString(DefaultValues.PROPERTY_ALGORITHM, DefaultValues.DEFAULT_HASHING_ALHORITHM);
            int iterationNb = sharedPref.getInt(DefaultValues.PROPERTY_ITERATIONS, DefaultValues.DEFAULT_ITERATIONS);
            int password_length = sharedPref.getInt(DefaultValues.PROPERTY_PASSWORD_LENGTH, DefaultValues.DEFAULT_PASSWORD_LENGTH);

            // get the hash algorithms interface
            HashInterface hash_algorithm = ImplementedHashingAlgorithms.getAlgorithmByName(algo);

            // get the users inputs
            String string_password = params[0];
            String string_salt = params[1];

            // hash it!
            byte[] input = hash_algorithm.hash(string_password, string_salt, Math.pow(2, iterationNb));

            // encode the result in base64 to make it readable
            String result = Base64.encodeToString(input, Base64.DEFAULT);

            // cut that strange "="-thing from the end of the result
            String result_cleanded = result.substring(0, result.length() - 2);

            // if password is to long -> cut it to the requested length
            if(password_length < result_cleanded.length())result_cleanded = result.substring(0, password_length);

            // thats it!
            return result_cleanded;
        }

        protected void onPostExecute(String result) {
            // save the password in the model
            strongerPassword = result;

            // set status to RESULT
            setStatus(MainModel.Status.RESULT);
        }

    }

}
