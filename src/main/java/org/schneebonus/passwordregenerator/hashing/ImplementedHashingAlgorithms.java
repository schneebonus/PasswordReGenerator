package org.schneebonus.passwordregenerator.hashing;

import org.schneebonus.passwordregenerator.hashing.PBKDF2.PBKDF2WithHmacSHA1;
import org.schneebonus.passwordregenerator.hashing.sha_and_md5.MD5;
import org.schneebonus.passwordregenerator.hashing.sha_and_md5.SHA1;
import org.schneebonus.passwordregenerator.hashing.sha_and_md5.SHA256;
import org.schneebonus.passwordregenerator.hashing.sha_and_md5.SHA512;

/**
 * Created by Mark Schneemann on 12.08.15.
 */
public class ImplementedHashingAlgorithms {
    // A list of all implemented and useable hash algorithms
    // TODO: simply add an object of a new hash algorithm here to make it useable
    private static HashInterface[] implementedHashingAlgorithms = {
                                                    new SHA512(),
                                                    new SHA256(),
                                                    new SHA1(),
                                                    new MD5(),
                                                    new PBKDF2WithHmacSHA1()
                                                    };

    public static String[] getImplenetedAlgorithmNames(){
        String[] names = new String[implementedHashingAlgorithms.length];

        for(int i = 0; i < implementedHashingAlgorithms.length; i++){
            names[i] = implementedHashingAlgorithms[i].getName();
        }
        return names;
    }

    public static HashInterface getAlgorithmByName(String name){
        for(HashInterface algorithm: implementedHashingAlgorithms){
            if(algorithm.getName().toString().equals(name.toString())){
                return algorithm;
            }
        }
        return implementedHashingAlgorithms[0];
    }

    public static boolean isAlgorithmImplemented(String name){
        for(HashInterface algorithm: implementedHashingAlgorithms){
            if(algorithm.getName().equals(name)){
                return true;
            }
        }
        return false;
    }
}
