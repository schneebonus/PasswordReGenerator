package org.schneebonus.passwordregenerator.hashing.sha_and_md5;

/**
 * Created by Mark Schneemann on 12.08.15.
 */
public class MD5 extends SHA_Base_Class{
    @Override
    public String getName() {
        return "MD5";
    }
}
