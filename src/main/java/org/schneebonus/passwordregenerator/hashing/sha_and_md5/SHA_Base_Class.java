package org.schneebonus.passwordregenerator.hashing.sha_and_md5;

import org.schneebonus.passwordregenerator.hashing.HashInterface;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Mark Schneemann on 12.08.15.
 */
public class SHA_Base_Class implements HashInterface {

    @Override
    public byte[] hash(String password, String salt, double iterations) {
        MessageDigest digest = null;
        byte[] input = null;
        try {
            digest = MessageDigest.getInstance(getName());
            digest.reset();
            digest.update(salt.getBytes());
            input = digest.digest(password.getBytes());
            for (int i = 0; i < iterations; i++) {
                digest.reset();
                input = digest.digest(input);
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return input;
    }

    @Override
    public String getName() {
        return "";
    }
}
