package org.schneebonus.passwordregenerator.hashing;

/**
 * Created by Mark Schneemann on 12.08.15.
 */
public interface HashInterface {
    public byte[] hash(String password, String salt, double iterations);
    public String getName();
}
