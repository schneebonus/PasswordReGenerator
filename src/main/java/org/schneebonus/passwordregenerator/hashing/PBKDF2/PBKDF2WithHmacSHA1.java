package org.schneebonus.passwordregenerator.hashing.PBKDF2;

import org.schneebonus.passwordregenerator.hashing.HashInterface;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * Created by Mark Schneemann on 13.08.15.
 */
public class PBKDF2WithHmacSHA1 implements HashInterface {
    public static final String PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA1";
    public static final int HASH_BYTE_SIZE = 24;

    @Override
    public byte[] hash(String password, String salt, double iterations) {
        if(password.length() == 0)password = "empty";
        if(salt.length() == 0)salt = "empty";

        // Hash the password
        byte[] hash = new byte[0];

        try {
            SecureRandom random = new SecureRandom();
            byte[] salt_bytes = salt.getBytes();
            char[] password_bytes = password.toCharArray();
            hash = pbkdf2(password_bytes, salt_bytes, (int)iterations, HASH_BYTE_SIZE);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

        return hash;
    }

    @Override
    public String getName() {
        return PBKDF2_ALGORITHM;
    }

    private static byte[] pbkdf2(char[] password, byte[] salt, int iterations, int bytes)
            throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
        return skf.generateSecret(spec).getEncoded();
    }
}
